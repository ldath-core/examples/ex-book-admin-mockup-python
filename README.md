# book-admin-mock

This is a Mockup service that will be used as a replacement for 
https://gitlab.com/ldath-core/examples/book-admin-mock
This is a super simple mock that was created just for the example.
You should create a more intelligent solution than this one.

## Requirements

This project requires Python == 3.10 and `pipenv` installed

You can install pipenv with brew https://brew.sh/:

```sh
    brew install pipenv
```

or in any other way: https://pipenv.pypa.io/en/latest/:

```sh
    pip install pipenv
```

## Configuration procedure

This project does not require any configuration at the moment

## Development

We are currently using json files to extend this mockup.

You just need to install dependencies with:

```sh
    pipenv install
```

### Service Development and Continuous Integration

- `serve.sh` - to serve it

additional flags which can be passed as arguments to `serve.sh`:

- `debug` - to enable debug logging
- `cors` - to enable cors

you can also use `docker-compose.yml` file created for this mock

## Test Mock Queues logic

The server is based on API declared in `https://gitlab.com/mobica-workshops/examples/go/gin/book-admin/-/tree/master/public/v1` API.
To improve the possibility to define from the test environment API test results, the Mock Server was extended by adding one more endpoint:


```sh
.../v1//mock-queue
```

Based on the received via a **POST** request, is pushing data to separate FIFO queues, which later are returned for each of **GET**/ **DELETE**/ **PUT** requests.

### Other materials

You can also read materials available here:

- https://sourcery.ai/blog/python-best-practices/
- https://sourcery.ai/blog/python-docker/


## License
 
The MIT License (MIT)

Copyright (c) Mobica Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
