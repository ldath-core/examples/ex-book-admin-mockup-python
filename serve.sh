#!/usr/bin/env bash
set -e

# Confirm if the Python3 is available on machine
if ! [ -x "$(command  -v python3)" ]; then
    echo "Please install python3"
    exit -1
fi

source $(dirname "$0")/scripts/build_parse_args.sh

set_debug_flag $@

set_cors_flag $@

# Start in separated environment Flask app.
pipenv run flask run -p 8882
